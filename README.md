This is an official implementation of paper "Unsupervised and Adaptive Perimeter Intrusion Detector", at ICIP 2022. 

#### Requirements

- PyTorch == 1.4.0

- Python==3.7.6

- `./requirement.sh`

#### Prepare dataset

```pyth
prepare_data.sh dataset datapath
dataset: i_LIDS
datapath: the path that you want to save the data, i.e., ./dataset/i_LIDS/
```

#### Train the model

```pyth
Run python Train.py with parameters like dataset, frame length, width, etc.
```

#### Evaluate the model

```pyth
python Testing.py
dataset: i_LIDS
datapath: the path that you saved the data
version: experiment version
ckpt: the checkpoint step
expdir: the path that you saved the model checkpoint
```

Note: The code will be cleaned and commented soon.





