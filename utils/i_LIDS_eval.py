def f_beta(p, r, b=1.0):
    return ((1 + b) * p * r) / ((b * p) + r)

from sklearn.metrics._ranking import _binary_clf_curve
def i_LIDS_level(y_true, y_score, n=50, n_pre=0, n_post=0):
    fps_b, tps_b, thresholds = _binary_clf_curve(y_true, y_score, pos_label=1)
    optimal_idxs = np.where(np.r_[True, np.logical_or(np.diff(fps_b, 2), np.diff(tps_b, 2)), True])[0]
    thresholds = thresholds[optimal_idxs]
    thresholds = thresholds[::-1] # reverse array
    th = 0.001 # consecutive difference
    thresholds = np.delete(thresholds, np.argwhere(np.ediff1d(thresholds) <= th) + 1) # get optimal thresholds
    thresholds = thresholds[::-1] # reverse as before
    t_used = []
    recall = []
    precision = []
    tps = []
    intrusions, _ = groupones(y_true)
    for t in thresholds:
        y_pred = np.zeros(y_true.shape[0])
        y_pred[y_score >= t] = 1
        tp = 0
        fn = 0
        to_del = []
        for intrusion in intrusions:
            if (calc_rising_edges(y_pred[intrusion[0] - n_pre: intrusion[0] + n]) >= 1):
                tp += 1  # if atleast 1 alarm in IBN
            else:
                fn += 1  # if no alarm in IBN
            if (intrusion[0] + n >= len(y_pred)):
                to_del.extend(np.arange(intrusion[0] - n_pre, len(y_pred) - 1))
            else:
                to_del.extend(np.arange(intrusion[0] - n_pre,  intrusion[0] + n))
        y_pred = make_rising_array(y_pred)
        y_pred[to_del] = 0
        fp = (y_pred == 1).sum()
        itemindex = np.where(y_pred == 1)
        dist_fp = np.ediff1d(itemindex)  # consecutive difference
        extra_fp = (dist_fp <= 25).sum()
        fp = fp - extra_fp# number of alarms outside IN
        tn = 1
        tpr_t = tp / (tp + fn)
        fpr_t = fp / (fp + tn)
        if (tp + fp) != 0:
            recall_t = tpr_t
            precision_t = tp / (tp + fp)
            recall.append(recall_t)
            precision.append(precision_t)
            tps.append(tp)
            t_used.append(t)
    print("Calculating event level precision and recall")
    tps = np.asarray(tps)
    recall = np.asarray(recall)
    precision = np.asarray(precision)
    t_used = np.asarray(t_used)
    # sort_inr = np.argsort(-recall)
    # precision = precision[sort_inr]
    # recall = recall[sort_inr]
    # precision = np.r_[precision, 1]
    # recall = np.r_[recall, 0]
    # aupr = auc(recall, precision)
    # print("PR", aupr)
    return t_used, recall, precision
